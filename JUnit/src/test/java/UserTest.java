import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {
    private static User user;


    @BeforeAll
    public static void createNewUsers() {
        user = new User("Frozen", "Lviv", 28);

    }

    @Test
    void ifUserAdultTest() throws IfNotAdultException {
        assertTrue(user.getAge() > 18);
    }

    @Test
    void editFullNameTest() {
        String aspect = "Andrew";
        user.editFullName(aspect);
        assertEquals(aspect, user.getFullName());
    }

    @Test
    void deleteAddressTest() {
        user.deleteAddress();
        assertEquals(null, user.getAddress());
    }

    @Test
    void addPetToListTest() {
        user.addPetToList("Cat");
        user.addPetToList("Dog");
        assertEquals("Dog", user.getPets().get(user.getPets().size()-1));

    }

    @org.junit.jupiter.api.Test
    void removePetFromListTest() {
        user.addPetToList("Cat");
        assertTrue(user.removePetFromList("Cat"));
    }
}