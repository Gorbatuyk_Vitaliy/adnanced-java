public class Main {
    public static void main(String[] args) throws IfNotAdultException {
        User user1 = new User("Frozen", "Lviv", 28);
        User user2 = new User("Mykola", "Kyiv", 16);
        User user3 = new User("Viktor", "Frankivsk", 20);



        User.ifUserAdult(user1);
        User.ifUserAdult(user2);
        User.ifUserAdult(user3);

        System.out.println(" ");
        user1.addPetToList("dog");
        user1.addPetToList("cat");
        System.out.println(user1.getPets());
    }
}

