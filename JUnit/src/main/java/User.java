import java.util.ArrayList;
import java.util.List;

public class User {
    private String fullName;
    private String address;
    private int age;
     List<String> pets = new ArrayList<String>();


    public User(String fullName, String address, int age) {
        this.fullName = fullName;
        this.address = address;
        this.age = age;
    }

    public static void ifUserAdult(User user) throws IfNotAdultException {

        if (user.age > 18) {
            System.out.println("User " + user.getFullName() + " is adult!");
        } else {
            throw new IfNotAdultException("User " + user.getFullName() + " isn't adult!");
        }
    }

    public void editFullName(String fullName) {
        this.fullName = fullName;
    }

    public void deleteAddress() {
        this.address = null;
    }

    public void addPetToList(String name) {
        this.pets.add(name);
    }

    public boolean removePetFromList(String name) {
        for (int i = 0; i < pets.size(); i++) {

            if (!pets.get(i).equals(name)) {
                System.out.println("Pet " + name + " doesn't exist in the list");
            } else {
                pets.remove(i);
                return true;
            }
        }
        return false;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAddress() {
        return address;
    }

    public int getAge() {
        return age;
    }

    public List<String> getPets() {
        return pets;
    }
}
