public class IfNotAdultException extends Exception {
    public IfNotAdultException(String errorMassage) {
        super(errorMassage);
    }
}
