package myClasses;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Tags {
    int id;
    String reference;

    public Tags(int id, String reference) {
        this.id = id;
        this.reference = reference;
    }
}
