package myClasses;

import lombok.Builder;//wont package-privet access
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Articles {
private int id;
private String title;
private String text;
private int idBlog;
ArrayList<Blogs>listOfBlogs;

    public Articles(int id, String title, String text, int idBlog) {
        this.id = id;
        this.title = title;
        this.text = text;
    }
    public Articles(){
    }

    public ArrayList<Blogs> getListOfBlogs() {
        return listOfBlogs;
    }

    public void setListOfBlogs(ArrayList<Blogs> listOfBlogs) {
        this.listOfBlogs = listOfBlogs;
    }

    @Override
    public String toString() {
        return "Articles{" +
                "id=" + id +
                ", '\n title='" + title +
                ", '\ntext='" + text +
                ", '\nidBlog=" + idBlog +
                ", '\nlistOfBlogs=" + listOfBlogs +
                '}';
    }
}
