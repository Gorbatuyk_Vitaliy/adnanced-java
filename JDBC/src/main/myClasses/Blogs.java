package myClasses;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Blogs {
    int id;
    String name;

    public Blogs(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
