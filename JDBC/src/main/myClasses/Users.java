package myClasses;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Users {
    int id;
    String full_name;

    public Users(int id, String full_name) {
        this.id = id;
        this.full_name = full_name;
    }
}
