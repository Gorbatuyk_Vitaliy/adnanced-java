import DaoClasses.ArticlesDao;
import DaoClasses.BlogsDao;
import DaoClasses.TagsDao;
import DaoClasses.UsersDao;
import lombok.SneakyThrows;
import myClasses.Articles;
import myClasses.Blogs;
import myClasses.Tags;
import myClasses.Users;

public class Main {
    @SneakyThrows
    public static void main(String[] args) {
        Articles articles = new Articles(1, "news 3", "afb areh NADA%DFNAGNA anndf agnf afngaG n",2);
//        Users user = new Users(3, "Vitalik");
//        Blogs blog = new Blogs(1, "My Blog 3");
//        Tags tag = new Tags(2, "www.some_tag_2.com");

       Articles newArticles = ArticlesDao.createObjectArticles(1);
       System.out.println(newArticles);


        //ArticlesDao.insertDataToDb(articles);
        //ArticlesDao.updateDataToDb(articles, 1);
        // ArticlesDao.deleteDataToDb(3);
        ArticlesDao.readFromDB();

        // UsersDao.insertDataToDb(user);
        //UsersDao.updateDataToDb(user, 2);
        // UsersDao.deleteDataToDb(1);
        UsersDao.readFromDB();

        //BlogsDao.insertDataToDb(blog);
        //BlogsDao.updateDataToDb(blog,1);
        //BlogsDao.deleteDataToDb(3);
        BlogsDao.readFromDB();

       //TagsDao.insertDataToDb(tag);
        //TagsDao.updateDataToDb(tag, 1);
        //TagsDao.deleteDataToDb(1);
        TagsDao.readFromDB();
    }
}
