package DaoClasses;

import myClasses.Tags;
import myClasses.Users;
import setting.ControlDB;

import java.sql.*;

public class UsersDao extends ControlDB {
    public static void readFromDB() throws Exception {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");
            Class.forName("com.mysql.cj.jdbc.Driver");
            while (resultSet.next()) {
                System.out.println("id = " + resultSet.getInt("id"));
                System.out.println("fullName = " + resultSet.getString("ful_name"));
                System.out.println(" ");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void insertDataToDb(Users userOne) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO users(ful_name) VALUE (?);")) {
            statement.setString(1, userOne.getFull_name());
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void updateDataToDb(Users userOne,int  id) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("UPDATE  users\n" +
                     "    SET ful_name= ? \n" +
                     "    WHERE id = ?")) {
            statement.setString(1, userOne.getFull_name());
            statement.setInt(2, id);
            int result = statement.executeUpdate();
            System.out.println(result);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void deleteDataToDb(int id) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM  users WHERE id = ?")) {
            statement.setInt(1, id);
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
