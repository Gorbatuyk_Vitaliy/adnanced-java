package DaoClasses;

import myClasses.Articles;
import myClasses.Blogs;
import setting.ControlDB;

import java.sql.*;
import java.util.ArrayList;

public class ArticlesDao extends ControlDB {

    public static Articles createObjectArticles(int id) throws Exception {
        Articles articles = new Articles();
        int idBlog;
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM articles WHERE id = ?")){
             statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            Class.forName("com.mysql.cj.jdbc.Driver");
            while (resultSet.next()) {
                articles.setId(resultSet.getInt("id"));
                articles.setTitle(resultSet.getString("title"));
                articles.setText(resultSet.getString("text"));
                articles.setIdBlog(resultSet.getInt("blog_id"));
                articles.setListOfBlogs(readListBlogs(resultSet.getString("title")));
                System.out.println(" ");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return articles;
    }

    private static ArrayList<Blogs> readListBlogs(String title) throws Exception {
        ArrayList<Blogs> listOfBlogs = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("select * from articles" +
                     " join blogs on articles.blog_id = blogs.id" +
                     " where articles.title = ?")) {
            statement.setString(1, title);
            ResultSet resultSet = statement.executeQuery();
            Class.forName("com.mysql.cj.jdbc.Driver");
            while (resultSet.next()) {
                listOfBlogs.add(new Blogs(resultSet.getInt("id"), resultSet.getString("name")));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listOfBlogs;
    }

    public static void readFromDB() throws Exception {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM articles");
            Class.forName("com.mysql.cj.jdbc.Driver");
            while (resultSet.next()) {
                System.out.println("id = " + resultSet.getInt("id"));
                System.out.println("title = " + resultSet.getString("title"));
                System.out.println("text = " + resultSet.getString("text"));
                System.out.println(" ");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void insertDataToDb(Articles articles) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO articles(title, text) VALUE (?, ?,?);")) {
            statement.setString(1, articles.getTitle());
            statement.setString(2, articles.getText());
            statement.setInt(3, articles.getIdBlog());
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void updateDataToDb(Articles articles, int id) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("UPDATE  articles\n" +
                     "    SET title= ?, text= ?, blog_id = ? \n" +
                     "    WHERE id = ?")) {
            statement.setString(1, articles.getTitle());
            statement.setString(2, articles.getText());
            statement.setInt(3, articles.getIdBlog());
            statement.setInt(4, id);
            int result = statement.executeUpdate();
            System.out.println(result);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void deleteDataToDb(int id) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM  articles WHERE id = ?")) {
            statement.setInt(1, id);
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
