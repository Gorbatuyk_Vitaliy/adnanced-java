package DaoClasses;

import myClasses.Blogs;
import setting.ControlDB;

import java.sql.*;

public class BlogsDao extends ControlDB  {
    private static int id = 3;

    public static void readFromDB() throws Exception {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM blogs");
            Class.forName("com.mysql.cj.jdbc.Driver");
            while (resultSet.next()) {
                System.out.println("id = " + resultSet.getInt("id"));
                System.out.println("name = " + resultSet.getString("name"));
                System.out.println(" ");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void insertDataToDb(Blogs blog) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO blogs(name) VALUE (?);")) {
            statement.setString(1, blog.getName());
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void updateDataToDb(Blogs blog,int id) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("UPDATE  blogs\n" +
                     "    SET  name= ? \n" +
                     "    WHERE id = ?")) {
            statement.setString(1, blog.getName());
            statement.setInt(2, id);
            int result = statement.executeUpdate();
            System.out.println(result);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void deleteDataToDb(int id) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM  blog WHERE id = ?")) {
            statement.setInt(1, id);
            int resultSet = statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
