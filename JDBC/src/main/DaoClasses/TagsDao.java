package DaoClasses;

import myClasses.Tags;
import setting.ControlDB;
import java.sql.*;

public class TagsDao extends ControlDB {

    public static void readFromDB() throws Exception {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM tags");
            Class.forName("com.mysql.cj.jdbc.Driver");
            while (resultSet.next()) {
                System.out.println("id = " + resultSet.getInt("id"));
                System.out.println("reference = " + resultSet.getString("reference"));
                System.out.println(" ");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void insertDataToDb(Tags tag) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("INSERT INTO tags(id, reference) VALUE (?, ?);")) {
            statement.setInt(1, tag.getId());
            statement.setString(2, tag.getReference());
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void updateDataToDb(Tags tag,int  id) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("UPDATE  tags\n" +
                     "    SET id= ?, reference= ? \n" +
                     "    WHERE id = ?")) {
            statement.setInt(1, tag.getId());
            statement.setString(2, tag.getReference());
            statement.setInt(3, id);
            int result = statement.executeUpdate();
            System.out.println(result);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public static void deleteDataToDb(int id) {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement("DELETE FROM  tags WHERE id = ?")) {
            statement.setInt(1, id);
            int resultSet = statement.executeUpdate();
            System.out.println(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
