import setting.ControlDB;

import java.sql.*;

public class TestObjectDao extends ControlDB {
    public static void validConnection() throws Exception {
        try (Connection connection = DriverManager.getConnection(url, user, password);
             Statement statement = connection.createStatement()) {
            Connection resultSet = statement.getConnection();
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
